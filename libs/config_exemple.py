#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# version 1
__version__ = '1'

# for print debug message
debug = False

# WIFI
essid = 'ESSID'
pw = 'PASSWORD'
hostname = 'doorbelltest'

# IFTT (XXXXX= your device, YYYYYYYYYYYYYYYY your keys webhook )
# or '' if no ifft event
# webhook = 'https://maker.ifttt.com/trigger/XXXXXXXX/with/key/YYYYYYYYYYYYYYYYYYYYYY'
webhook = ''

# jeedom (attention at {} a the end of jeedom link)
jeedom = "http://192.168.0.202:8000/core/api/jeeApi.php?plugin=virtual&apikey=iyqw8N6cRZKSANaJ1vFkRWWybqvaXXsL&type=virtual&id=715&value={}"

# button and buzzer pins
pin_button = 27
pin_buzzer = 23

# pins MIN132 card
pin_output = [2, 4, 5, 12, 13, 14, 15, 16, 17, 18, 19,
              21, 22, 23, 25, 26, 27, 32, 33]

pin_input = [2, 4, 5, 13, 14, 15, 16, 17, 18, 19,
             21, 22, 23, 25, 26, 27, 32, 33, 34,
             35, 36, 39]
