from machine import Pin, PWM
import time
from libs.musique import music


def play_musique(pin=2):
    """Play mario sounds."""
    beeper = PWM(Pin(pin, Pin.OUT), freq=440, duty=512)
    for i in music:
        beeper.freq(i)
        time.sleep_ms(150)
    beeper.deinit()

if __name__ == '__main__':
    play_musique(23)
