#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# version 1
__version__ = '1'


print("let's go!!! Ring my bell!!! (thanks Anita)")

import libs.config as config
import network


wlan = network.WLAN(network.STA_IF)
wlan.active(True)
if not wlan.isconnected():
    print('connecting to network...')
    wlan.connect(config.essid, config.pw)
wlan.config(dhcp_hostname=config.hostname)
print('network config:', wlan.ifconfig())
