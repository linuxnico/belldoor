#!/usr/bin/env micropython

# import machine
# import network
import libs.tinyweb as tinyweb

# Create web server
# import _thread
app = tinyweb.webserver()


# Index page
@app.route('/')
@app.route('/index.html')
async def index(req, resp):
    print(type(req.query_string))
    print(req.query_string)
    # if req.query_string != b'':
    #     music = req.query_string.decode().split('=')[1]
    #     print(music)
    #     with open('libs/musique.py', 'w') as f:
    #         f.write('from libs.notes import *\n')
    #         f.write('music = {}'.format(music))
    #     await resp.start_html()
    #     await resp.send("<html><body><h1>c'est fait!!!</h1></body></html>\n")
    #     # return
    await resp.send_file('index.html')


def run():
    app.run(host='0.0.0.0', port=80)


if __name__ == '__main__':
    run()
