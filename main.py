#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# version 1
__version__ = '1'

import _thread
import libs.urequests as urequests
from machine import Pin
from time import sleep
import libs.config as config
import libs.sounds as sounds


def call_ifttt():
    """Call ifttt for sending info."""
    try:
        serveur = config.webhook
        if serveur != '':
            _ = urequests.get(serveur)
    except Exception:
        pass


def call_jeedom():
    """Call jeedom for sending info."""
    try:
        serveur = config.jeedom
        if serveur != '':
            _ = urequests.get(serveur.format(1))
            sleep(1)
            _ = urequests.get(serveur.format(0))
    except Exception:
        pass


def callInternet():
    """Ring a bell."""
    global wlan
    # sounds.play_mario(config.pin_buzzer)
    if wlan.isconnected():
        print('IFTTT')
        call_ifttt()
        print('JEEDOM')
        call_jeedom()


def ring():
    sounds.play_musique(config.pin_buzzer)


button = Pin(config.pin_button, Pin.IN, Pin.PULL_UP)


def onsonne(pin):
    print('on sonne peut etre')
    print(wlan.ifconfig())
    sleep(0.1)
    _thread.start_new_thread(ring, ())
    if not wlan.isconnected():
        print('connecting to network...')
        wlan.connect(config.essid, config.pw)
    callInternet()
    print('exit ring')


button.irq(trigger=Pin.IRQ_RISING, handler=onsonne)
import server
server.run()
